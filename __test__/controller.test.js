const request = require('supertest');
const express = require('express');
const { getInventory, createInventory, updateInventory, deleteInventory } = require('../service/inventoryService/controllers/controller'); // Adjust the path as necessary

jest.mock('../db'); // Mock the database connection

const connection = require('../db');

const app = express();
app.use(express.json());

// Configure routes
app.get('/inventory', getInventory);
app.post('/inventory', createInventory);
app.put('/inventory/:productId', updateInventory);
app.delete('/inventory/:productId', deleteInventory);

describe('Inventory API', () => {
  beforeEach(() => {
    connection.query.mockClear();
  });

  describe('GET /inventory', () => {
    test('should return inventory data', async () => {
      const mockData = [
        { categoryOfEquipment: 'Cat1', location: 'Loc1', availableQty: 10, ownerMail: 'owner1@example.com' }
      ];
      // Mocking the database query result
      connection.query.mockImplementation((sql, callback) => {
        callback(null, mockData);
      });

      const response = await request(app).get('/inventory');

      expect(response.status).toBe(200);
      expect(response.body).toEqual(mockData);
    });

    test('should return 500 on query error', async () => {
      // Mocking query error scenario
      connection.query.mockImplementation((sql, callback) => {
        callback(new Error('Query error'), null);
      });

      const response = await request(app).get('/inventory');

      expect(response.status).toBe(500);
      expect(response.body).toEqual({ error: "Failed to get data" });
    });

    test('should return 404 if no data found', async () => {
      // Mocking empty result scenario
      connection.query.mockImplementation((sql, callback) => {
        callback(null, []);
      });

      const response = await request(app).get('/inventory');

      expect(response.status).toBe(404);
      expect(response.body).toEqual({ message: 'Inventory Details not found' });
    });
  });

  describe('POST /inventory', () => {
    test('should create new inventory items', async () => {
      const items = [
        { name: 'Item1', categoryOfEquipment: 'Cat1', totalQty: 10, availableQty: 10, location: 'Loc1', description: 'Desc1', ownerMail: 'owner1@example.com' }
      ];

      connection.query.mockImplementation((sql, values, callback) => {
        callback(null, { affectedRows: 1 });
      });

      const response = await request(app)
        .post('/inventory')
        .send(items);

      expect(response.status).toBe(201);
      expect(response.body).toEqual({ message: "Created successfully in inventory" });
    });

    test('should return 400 if request body is not an array', async () => {
      const response = await request(app)
        .post('/inventory')
        .send({ name: 'InvalidItem' });

      expect(response.status).toBe(400);
      expect(response.body).toEqual({ error: 'Request body should be an array of items' });
    });

    test('should return 500 on query error', async () => {
      connection.query.mockImplementation((sql, values, callback) => {
        callback(new Error('Query error'), null);
      });

      const items = [
        { name: 'Item1', categoryOfEquipment: 'Cat1', totalQty: 10, availableQty: 10, location: 'Loc1', description: 'Desc1', ownerMail: 'owner1@example.com' }
      ];

      const response = await request(app)
        .post('/inventory')
        .send(items);

      expect(response.status).toBe(500);
      expect(response.body).toEqual({ error: "Failed to create data" });
    });
  });

  describe('PUT /inventory/:productId', () => {
    test('should update the available quantity of an inventory item', async () => {
      connection.query.mockImplementation((sql, values, callback) => {
        callback(null, { affectedRows: 1 });
      });

      const response = await request(app)
        .put('/inventory/123')
        .send({ availableQty: 5 });

      expect(response.status).toBe(200);
      expect(response.body).toEqual({ message: "Data updated successfully" });
    });

    test('should return 400 if productId or availableQty is missing', async () => {
      const response = await request(app)
        .put('/inventory/123')
        .send({});

      expect(response.status).toBe(400);
      expect(response.body).toEqual({ error: 'productId and availableQty are required' });
    });

    test('should return 500 on query error', async () => {
      connection.query.mockImplementation((sql, values, callback) => {
        callback(new Error('Query error'), null);
      });

      const response = await request(app)
        .put('/inventory/123')
        .send({ availableQty: 5 });

      expect(response.status).toBe(500);
      expect(response.body).toEqual({ error: "Failed to update data" });
    });
  });

  describe('DELETE /inventory/:productId', () => {
    test('should delete an inventory item', async () => {
      connection.query.mockImplementation((sql, values, callback) => {
        callback(null, { affectedRows: 1 });
      });

      const response = await request(app)
        .delete('/inventory/123');

      expect(response.status).toBe(200);
      expect(response.body).toEqual({ message: "Deleted successfully" });
    });

    test('should return 500 on query error', async () => {
      connection.query.mockImplementation((sql, values, callback) => {
        callback(new Error('Query error'), null);
      });

      const response = await request(app)
        .delete('/inventory/123');

      expect(response.status).toBe(500);
      expect(response.body).toEqual({ error: "Failed to delete data" });
    });
  });
});
