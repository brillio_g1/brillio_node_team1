const { generateTicketNumber } = require('../example'); // Adjust the path as necessary

describe('generateTicketNumber', () => {
  test('should return a string with prefix "TIC" followed by a 4-digit number', () => {
    const ticketNumber = generateTicketNumber();
    expect(ticketNumber.startsWith('TIC')).toBe(true);
    const numberPart = ticketNumber.slice(3);
    expect(!isNaN(numberPart)).toBe(true);
    expect(numberPart.length).toBe(4);
    const numericValue = parseInt(numberPart, 10);
    expect(numericValue).toBeGreaterThanOrEqual(1000);
    expect(numericValue).toBeLessThanOrEqual(9999);
  });
});
