function generateTicketNumber() {
    const prefix = 'TIC';
    const uniqueId = Math.floor(1000 + Math.random() * 9000);
    return `${prefix}${uniqueId}`;
}

module.exports = { generateTicketNumber };
