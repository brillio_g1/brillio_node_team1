const express = require("express");
const cors = require('cors');
const inventoryRouter = require("./inventoryService/routes/routes");
const app = express();
const siteRouter = require('./siteService/routes/routes');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const router = express.Router();

var corsOptions = {
    origin: true,
    methods: ["POST", "GET", "PUT", "DELETE"],
    credentials: true,
    maxAge: 3600
};
app.use(cors(corsOptions));

app.use('/inventory', inventoryRouter);
app.use('/sites', siteRouter)
app.use('/', router);

const PORT = process.env.PORT || 3000;
app.listen(PORT, function () {
    console.log(`Listening on server at port ${PORT}`);
});
