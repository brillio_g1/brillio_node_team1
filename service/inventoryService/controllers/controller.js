const connection = require('../../../db');

function generateProductId() {
    const prefix = 'PRDOUCT';
    const uniqueId = Math.floor(1000 + Math.random() * 2000);
    return `${prefix}${uniqueId}`;
}

exports.getInventory = async (req, res) => {
    try {
        let sql = 'SELECT categoryOfEquipment, location, availableQty, ownerMail FROM node_team_1.inventory';

        connection.query(sql,(err, result) => {
            if (err) {
                console.error('Error executing query', err);
                return res.status(500).json({ error: "Failed to get data" });
            }
            if (result.length) {
                res.status(200).json(result);
            } else {
                res.status(404).json({ message: 'Inventory Details not found' });
            }
        });
    } catch (error) {
        console.error('Unexpected error', error);
        res.status(500).json({ error: "Internal server error" });
    }
}

exports.createInventory = async (req, res) => {
    try {
        const items = req.body;
        if (!Array.isArray(items)) {
            return res.status(400).json({ error: 'Request body should be an array of items' });
        }

        const sql = "INSERT INTO node_team_1.inventory (productId, name, categoryOfEquipment, totalQty, availableQty, location, description, ownerMail) VALUES ?";
        const values = items.map(item => [
            item.productId = generateProductId(),
            item.name,
            item.categoryOfEquipment,
            item.totalQty,
            item.availableQty,
            item.location,
            item.description,
            item.ownerMail
        ]);

        connection.query(sql, [values], (err, result) => {
            if (err) {
                console.error('Error executing query', err);
                return res.status(500).json({ error: "Failed to create data" });
            }
            res.status(201).json({ message: "Created successfully in inventory" });
        });
    } catch (error) {
        console.error('Unexpected error', error);
        res.status(500).json({ error: "Internal server error" });
    }
}

exports.updateInventory = async (req, res) => {
    try {
        const { productId } = req.params;
        const { availableQty } = req.body;

        if (!productId || availableQty === undefined) {
            return res.status(400).json({ error: 'productId and availableQty are required' });
        }

        const sql = "UPDATE node_team_1.inventory SET availableQty = ? WHERE productId = ?";
        connection.query(sql, [availableQty, productId], (err, result) => {
            if (err) {
                console.error('Error executing query', err);
                return res.status(500).json({ error: "Failed to update data" });
            }
            res.status(200).json({ message: "Data updated successfully" });
        });
    } catch (error) {
        console.error('Unexpected error', error);
        res.status(500).json({ error: "Internal server error" });
    }
}

exports.deleteInventory = async (req, res) => {
    try {
        const { productId } = req.params;

        if (!productId) {
            return res.status(400).json({ error: 'productId is required' });
        }

        const sql = "DELETE FROM node_team_1.inventory WHERE productId = ?";
        connection.query(sql, [productId], (err, result) => {
            if (err) {
                console.error('Error executing query', err);
                return res.status(500).json({ error: "Failed to delete data" });
            }
            res.status(200).json({ message: "Deleted successfully" });
        });
    } catch (error) {
        console.error('Unexpected error', error);
        res.status(500).json({ error: "Internal server error" });
    }
}
