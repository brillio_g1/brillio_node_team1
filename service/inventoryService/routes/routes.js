const express = require('express');
const inventoryService = require('../controllers/controller');

const inventoryRouter = express.Router();

inventoryRouter.get('/output', inventoryService.getInventory);
inventoryRouter.post('/input', inventoryService.createInventory);
inventoryRouter.put('/update/:productId', inventoryService.updateInventory);
inventoryRouter.delete('/delete/:productId', inventoryService.deleteInventory);


module.exports = inventoryRouter;
