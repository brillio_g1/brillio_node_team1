const connection = require('../../../db');
const { send } = require('../../utilities/mailservice');

// Adding a site data to DB
const addSiteData = async (req, res) => {
    try {
        // calling the ticket number genaration funciton
        const newTicketNumber = ticketNumberGenerator();

        const {
            siteId,
            siteName,
            userName,
            categoryOfEquipment,
            ownerName,
            location,
            details,
            ownerMail,
        } = req.body;

        // framing the query
        let sql =
            "INSERT INTO testdb.sites (siteId, siteName, userName, categoryOfEquipment, ownerName, location, details, ownerMail) VALUES (?,?,?,?,?,?,?,?)";

        // making the connection call to DB
        connection.query(
            sql,
            [
                siteId,
                siteName,
                userName,
                categoryOfEquipment,
                ownerName,
                location,
                details,
                ownerMail,
            ],
            function (err, result) {
                if (result) {
                    const sql =
                        "UPDATE testdb.sites SET ticketNumber = " +
                        '"' +
                        newTicketNumber +
                        '"' +
                        " WHERE siteId = " +
                        req.body.siteId +
                        ";";
                    connection.query(sql);
                    // sending mail confirmation
                    const mailDetails = {
                        ownerName: ownerName,
                        ownerMail: ownerMail,
                        ticketNumber: newTicketNumber,
                        siteName: siteName,
                        location: location,
                        fromOp: "create"
                    }
                    send(mailDetails);
                    res.send(
                        `The site data is Saved with the ticket number ${newTicketNumber}`
                    );
                }
                if (err) throw err.message;
            }
        );
    } catch (err) {
        res.status(400).send({ error: err });
    }
};

// Getting all the sites data
const getAllSites = async (req, res) => {
    // query formation
    console.log('sites all data');
    const sql = "select * from testdb.sites";

    try {
        connection.query(sql, function (err, result) {
            if (result) {
                connection.query(sql);
                res.send(result);
            }
            if (err) throw err.message;
        });
    } catch (err) {
        res.status(400).send({ error: err });
    }
};

// Get a single/specific site data based on site id
const getSiteData = async (req, res) => {
    try {
        // formation of the query
        const sql = "select * from testdb.sites where siteId = " + req.params.id + "";

        connection.query(sql, function (err, result) {
            if (result) {
                connection.query(sql);
                res.send(result);
            }
            if (err) throw err.message;
        });
    } catch (err) {
        res.status(400).send({ error: err });
    }
};

// Updating data for a specific site only
const updateSiteData = async (req, res) => {
    try {
        const conditions = [];
        const updateFields = [];
        const queryParams = [];

        // query formation
        let sql = "update testdb.sites set";

        // calling the ticket number genaration funciton
        const newTicketNumber = ticketNumberGenerator();

        const {
            siteName,
            userName,
            categoryOfEquipment,
            ownerName,
            location,
            details,
            ownerMail,
        } = req.body;

        // update fields
        if (siteName) {
            updateFields.push('siteName = ?');
            queryParams.push(siteName);
        }

        if (userName) {
            updateFields.push('userName = ?');
            queryParams.push(userName);
        }

        if (categoryOfEquipment) {
            updateFields.push('categoryOfEquipment = ?')
            queryParams.push(categoryOfEquipment);
        }

        if (ownerName) {
            updateFields.push('ownerName = ?')
            queryParams.push(ownerName);
        }

        if (location) {
            updateFields.push('location = ?')
            queryParams.push(location);
        }

        if (details) {
            updateFields.push('details = ?')
            queryParams.push(details);
        }

        if (ownerMail) {
            updateFields.push('ownerMail = ?')
            queryParams.push(ownerMail);
        }

        // Joining update fields with commas
        sql += ' ' + updateFields.join(', ');

        // conditions
        if (req.params.id) {
            conditions.push('siteId = ?');
            queryParams.push(req.params.id);
        }

        // Joining conditions with AND if there are any
        if (conditions.length > 0) {
            sql += ' WHERE ' + conditions.join(' AND ');
        }

        connection.query(sql, queryParams, (err, results) => {
            if (results) {
                const sql =
                    "UPDATE testdb.sites SET ticketNumber = " +
                    '"' +
                    newTicketNumber +
                    '"' +
                    " WHERE siteId = " +
                    req.params.id +
                    ";";
                connection.query(sql);
                const mailDetails = {
                    ownerName: ownerName,
                    ownerMail: ownerMail,
                    ticketNumber: newTicketNumber,
                    siteName: siteName,
                    location: location,
                    fromOp: "update"
                }
                // send a ,eil here
                send(mailDetails);
                res.status(200).json({
                    Message: "The data has been updated for the given site.",
                    "Ticket Number": newTicketNumber,
                    "User Name": userName,
                    "Owner Name": ownerName,
                });
            }
            if (err) throw err.message;
        });
    } catch (err) {
        res.status(400).send({ error: err });
    }
}

// Delete a site from DB
const deleteSiteData = async (req, res) => {
    try {
        // query formation
        const sql =
            "select * from testdb.sites where siteId = " + req.params.id + "";

        connection.query(sql, function (err, result) {
            if (result) {
                let siteName = result[0].siteName;
                let ownerName = result[0].ownerName;
                let ownerMail = result[0].ownerMail;

                const sql = "delete from testdb.sites where siteId = " + req.params.id + "";
                connection.query(sql);
                // send a mail here
                const mailDetails = {
                    ownerName: ownerName,
                    ownerMail: ownerMail,
                    siteName: siteName,
                    fromOp: "delete"
                }
                send(mailDetails);
                res.status(200).json({
                    "Message": "The site has been deleted.",
                    "Owner Name": ownerName
                })
            }
            if (err) throw err.message;
        });
    } catch (err) {
        res.status(400).send({ error: err });
    }
};


// Random ticket generation function
const ticketNumberGenerator = () => {
    const letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"];
    const randomLetter = Math.floor(Math.random() * letters.length);
    const numbers = Math.floor(Math.random() * 10000000);
    return `TN${letters[randomLetter].toUpperCase() + numbers}`;
};

module.exports = {
    addSiteData,
    getAllSites,
    getSiteData,
    updateSiteData,
    deleteSiteData,
    ticketNumberGenerator
}