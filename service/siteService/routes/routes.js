const express = require('express');
const siteService = require('../controllers/controller');

const siteRouter = express.Router();

    // @route   GET /api/sites/
    // @desc    Get all sites
    // @access  Public
    siteRouter.get('/', siteService.getAllSites);

    // @route   GET /api/sites/:id
    // @desc    Get a specific site
    // @access  Public
    siteRouter.get('/:id', siteService.getSiteData);

    // @route   POST /api/sites/
    // @desc    Create a site
    // @access  Public
    siteRouter.post('/', siteService.addSiteData);

    // @route   PUT /api/sites/:id
    // @desc    Update a site
    // @access  Public
    siteRouter.put('/:id', siteService.updateSiteData);

    // @route   DELETE /api/sites/:id
    // @desc    Delete a site
    // @access  Public
    siteRouter.delete('/:id', siteService.deleteSiteData);


module.exports = siteRouter;