const nodemailer = require("nodemailer");

var send = function (req, res) {
  var transport = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "asdfqweerrccb@limitlesscircle.com",
      pass: "qwerr@wee",
    },
  });
  
  let mailOptions = {}

  if(req.fromOp === 'create') {
    mailOptions = {
        from: "transactions@limitlesscircle.com",
        to: `${req.ownerMail}`,
        subject: "Site Creation",
        text: `Hello ${req.ownerName}, 
        The site ${req.siteName} has been created on ${req.location}. 
        Here is your ticket number : ${req.ticketNumber}.`,
      };
  }

  if(req.fromOp === 'update') {
    mailOptions = {
        from: "transactions@limitlesscircle.com",
        to: `${req.ownerMail}`,
        subject: "Site Updation",
        text: `Hello ${req.ownerName}, 
        The site ${req.siteName} has been updated. 
        Here is your ticket number : ${req.ticketNumber}.`,
    };
  }

  if(req.fromOp === 'delete') {
    mailOptions = {
        from: "transactions@limitlesscircle.com",
        to: `${req.ownerMail}`,
        subject: "Site Deletion",
        text: `Hello ${req.ownerName}, The site ${req.siteName} has been deleted.`,
    };
  }

  console.log(mailOptions)

  transport.sendMail(mailOptions, function (error, res) {
    console.log('send mail called from site service');
    if (error) {
      res.send("Email could not sent due to error: " + error);
      console.log(error);
    } else {
      res.send("Email has been sent successfully");
      console.log("mail sent");
    }
  });
};

module.exports = {
    send
}
